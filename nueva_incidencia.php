<?php
session_start();
if($_SESSION['usuario']!='' && $_SESSION['estatus']!=''){
?>

<!DOCTYPE html>
<html lang="es">

<?php
require_once "assets/component/cabecera.php";
?> 

<body class="fix-header card-no-border">
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <div id="main-wrapper">
        <header class="topbar">
            <nav class="navbar top-navbar navbar-toggleable-sm navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon -->
                        <b>
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <img src="assets/images/logo-icon.png" alt="homepage" class="dark-logo" />
                            <!-- Light Logo icon -->
                            <img src="assets/images/logo-light-icon.png" alt="homepage" class="light-logo" />
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <span>
                         <!-- dark Logo text -->
                         <img src="assets/images/logo-text.png" alt="homepage" class="dark-logo" />
                         <!-- Light Logo text -->    
                         <img src="assets/images/logo-light-text.png" class="light-logo" alt="homepage" /></span> </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0 ">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="icon-arrow-left-circle"></i></a> </li>
                        <!-- ============================================================== -->
                        <!-- Comment -->
                        <!-- ============================================================== -->
                        
                        <!-- ============================================================== -->
                        <!-- End Comment -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- Messages -->
                        <!-- ============================================================== -->
                       
                  
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
<!--
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="mdi mdi-message"></i>
                                <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
                            </a>
                            <div class="dropdown-menu mailbox animated bounceInDown">
                                <ul>
                                    <li>
                                        <div class="message-center">
                                          
                                            <a href="#">
                                                <div class="btn btn-danger btn-circle"><i class="fa fa-link"></i></div>
                                                <div class="mail-contnet">
                                                    <h5>Luanch Admin</h5> <span class="mail-desc">Just see the my new admin!</span> <span class="time">9:30 AM</span> </div>
                                            </a>
                                        </div>
                                    </li>
                                    <li>
                                        <a class="nav-link text-center" href="javascript:void(0);"> <strong>Check all notifications</strong> <i class="fa fa-angle-right"></i> </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                      -->  


                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span>Administrador</span></a>
                            <div class="dropdown-menu dropdown-menu-right animated flipInY">
                                <ul class="dropdown-user">
                                    <li><a href="#"><i class="fa fa-power-off"></i> Cerrar Sesion</a></li>
                                </ul>
                            </div>
                        </li>

                    </ul>
                </div>
            </nav>
        </header>

        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                       <li><a class="has-arrow" href="index.php" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard</span></a>
                    </li> 
                        <li>
                            <a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-file"></i><span class="hide-menu">Agregar Incidencia </span></a>
                        </li>  
                        <li>
                            <a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-table"></i><span class="hide-menu">Estadistica</span></a>
                        </li>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
      
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Agregar Incidencia</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Registro de Incidencias</a></li>
                        </ol>
                    </div>
                </div>
           
               
                <!-- .row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-block">
                            <div class="row">
                                
                                <div class="col-sm-12 col-xs-12">
                                    <form id="registro" name="registro" method="POST" action="">
                                        <div class="row">                       
                                            <div class="col-lg-6">
                                                <div class="input-group">
                                                <?php
                                                require_once "assets/component/clinica.php";
                                                ?>   
 
                                                </div>
                                                <div class="input-group">
                                                    <input type="text" class="form-control format" placeholder="Solicitante" id="solicitante" name="solicitante" required>
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <div class="input-group">
                                                    <textarea class="form-control" rows="4" placeholder="Descripci&oacute;n" id="descripcion" name="descripcion" required></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">                       
                                            <div class="col-lg-6 detail-group">
                                                <div class="input-group ">
                                                    <input type="text" class="form-control" placeholder="Causa del Problema" id=causa name="causa" required>
                                                </div>
                                                <div class="input-group format">
                                                    <?php
                                                    require_once "assets/component/area.php";
                                                    ?>
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <div class="input-group info">
                                                    <textarea class="form-control" rows="4" placeholder="Soluci&oacute;n" id="solucion" name="solucion" required></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row detail-group">                       
                                            <div class="col-lg-6"> 
                                                <div class="input-group" id="tipologia">
                                                <select class="custom-select col-12" id="tipologia" name="tipologia" required>
                                                <option value="" selected>Tipologia</option>    
                                                </select>
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <div class="input-group" id="sub_tipologia">
                                                <select class="custom-select col-12" id="sub" name="sub" required>
                                                <option value="" selected>Sub-Categoria</option>    
                                                </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row detail-group">                       
                                            <div class="col-lg-6"> 
                                                <div class="input-group">
                                                    <select class="custom-select col-12" id="prioridad" name="prioridad" required>
                                                        <option selected value="">Prioridad</option>
                                                        <option value="1">INMEDIATA</option>
                                                        <option value="2">ALTA</option>
                                                        <option value="3">MEDIA</option>
                                                        <option value="4">BAJA</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <div class="input-group">
                                                    <?php
                                                    require_once "assets/component/estatus.php";
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="save detail-group">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light" id="guardar">Guardar</button>
                                    </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>



                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-block">
                                <h3 class="text-themecolor m-b-0 m-t-0">Incidencias Registradas</h3>
                                <div class="table-responsive m-t-40" id="tabla_resumen">
                                    <?php
                                    require_once "assets/component/resumen_dia.php";
                                    ?>
                                </div>
                            </div>
                        </div>
                       
                       
                    </div>
                </div>



           </div>
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer">
                © 2021 Reporte Dentalia
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>

     <?php
     require_once "assets/component/modal_editar.php";
     require_once "assets/component/js.php";
     ?> 
</body>

</html>
<?php
}else{
    header("location:index.php");
}
?>
