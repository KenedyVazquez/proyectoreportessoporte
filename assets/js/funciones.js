//data table traduccion y botones de exportacion
$(document).ready(function() {
    $('#example23').DataTable({
            "order": [[ 0, "desc" ]],
          "paging": true,
          "lengthChange": false,
          "searching": true,
          "ordering": false,
          "info": true,
        "language": {
        "url": "https://cdn.datatables.net/plug-ins/1.11.3/i18n/es_es.json"
    },
     dom: 'Bfrtip',
     buttons: [
         'copy', 'excel'
     ]
    });
 });
 //fin de datatabla

 //tabla general
 //data table traduccion y botones de exportacion
$(document).ready(function() {
    $('#tabla_general').DataTable({
        "order": [[ 0, "desc" ]],
          "paging": true,
          "lengthChange": false,
          "searching": true,
          "ordering": false,
          "info": true,
        "language": {
        "url": "https://cdn.datatables.net/plug-ins/1.11.3/i18n/es_es.json"
    },
     dom: 'Bfrtip',
     buttons: [
         'copy', 'excel'
     ]
    });
 });
 //fin de datatabla

 //validaciones de campos
 
//-----------------validacion de caracteres
function soloLetras(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla == 8)
        return true;
    patron = /[A-Za-zñÑ \s]/;
    te = String.fromCharCode(tecla);
    return patron.test(te);
}

function numYletras(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla == 8)
        return true;
    patron = /[\w \s @ ñÑ.]/;
    te = String.fromCharCode(tecla);
    return patron.test(te);
}

function num(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla == 8)
        return true;
    patron = /[0-9.]/;
    te = String.fromCharCode(tecla);
    return patron.test(te);
}
// fin validaciones

//registrar usuario
$(document).ready(function(){
    $("#registrar").click(function (event){
      event.preventDefault();
      if($("#nombre").val()=="" || $("paterno").val()=="" || $("#materno").val()=="" || $("#usuario").val()=="" || $("#clave").val()==""){
          alertify.error("INGRESAR LA INFORMACION SOLICITADA");
      }else{
          $.ajax({
              type: "POST",
              url: "assets/php/registro_usuario.php",
              data: $("#registro_usuario").serialize(),

              success:function(respuesta){
                  if(respuesta==1){
                      alertify.success("REGISTRO EXITOSO");
                  }else if(respuesta==2){
                      alertify.error("ERROR AL REGISTRAR");
                  }else{
                      alertify.error("ERROR AL PROCESAR SOLICITUD");
                  }
              }//end success

          });//ennd ajax
      }//end else
    });//end click
});//end document
//fin de registro

// logeo
$(document).ready(function(){
    $("#logeo").click(function (event){
        event.preventDefault();
        if($("#usuario").val()=="" || $("#clave").val()==""){
            alertify.error("FALTAN DATOS");
        }else{
            $.ajax({
                type: "POST",
                url: "assets/php/autorizar.php",
                data: $("#login").serialize(),
                success:function(respuesta){
                    if(respuesta==1){
                        alertify.success("BIENVENIDO");
                        location.href="dashboard.php";
                    }else if(respuesta==2){
                        alertify.error("USUARIO LOGEADO EN OTRO DISPOSITIVO");
                    }else{
                        alertify.error("FAVOR DE VERIFICAR USUARIO Y CLAVE");
                    }
                }//end success
            });//end ajax
        }
    });
});
//fin logeo

//cerrar sesion

$("#cerrar").click(function(event){
    $cadena="confirmacion=si";
    event.preventDefault();
    $.ajax({
        type: "POST",
        url: "assets/php/cerrar_sesion.php",
        data: $cadena,

        success:function(respuesta){
            if(respuesta==1){
                location.href="index.php";
            }else{
                alertify.error("OCURRIO UN ERROR");
            }
        }
    });
});

 //limpiar campos
 function limpiar(){
    $("#clinica").val("");
    $("#solicitante").val("");
    $("#descripcion").val("");
    $("#causa").val("");
    $("#area").val("");
    $("#lista_tipologia").val("");
    $(".sub_tipo").val("");
    $("#prioridad").val("");
    $("#estatus").val("");
    $("#solucion").val("");
    }
 //fin de limpiar campos
 
//registro de incidencia
$(document).ready(function(){
    $("#guardar").click(function (event){
        event.preventDefault();
        if( $("#clinica").val()=="" || $("#solicitante").val()=="" || $("#descripcion").val()=="" || $("#causa").val()=="" || $("#area").val()=="" || $("#lista_tipologia").val()=="" || $(".sub_tipo").val()=="" || $("#prioridad").val()=="" || $("#estatus").val()=="" || $("#solucion").val()==""){
            alertify.error("faltan campos por llenar");
        }else{
            $.ajax({
                type: "POST",
                url: "assets/php/registro_incidencia.php",
                data: $("#registro").serialize(),
                success:function(respuesta){
                    if(respuesta==1){
                        alertify.success('Incidencia Registrada Correctamente');
                        limpiar();
                        $("#tabla_resumen").load("assets/component/resumen_actualizado.php");
                    }else{
                        alertify.error('Error al Registrar');
                    }
                }//end success
            });//end ajax
        }
    });
});
// fin de registro de incidencias

//eliminar registros
function eliminar(dato){
    var cadena="registro="+dato;
    Swal.fire({
        title: '¿ESTAS SEGURO DE ELIMINAR EL REGISTRO?',
        showCancelButton: true,
        confirmButtonText: 'Aceptar',
      }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
            $.ajax({
                type: "POST",
                url: "assets/php/eliminar_registro.php",
                data: cadena,
            
                success:function(respuesta){
                    if(respuesta==1){
                        alertify.success('Incidencia Eliminada Correctamente');
                        $("#tabla_resumen").load("assets/component/resumen_actualizado.php"); 
                        $("#tabla_indicadores").load("assets/component/indicadores_actualizado.php");
                    }else{
                        alertify.error('Error al Eliminar');
                    }
                }//end success
            });//end ajax
        } 
      })
}

// editar incidencia

function editar(dato){
    var envio="dato="+dato;

    $.ajax({
        type: "POST",
        data: envio,
        url: "assets/php/consulta_datos.php",

        success:function(respuesta){
            var info=respuesta.split("||");
            $("#clinica_act").val(info[2]);
            $("#solicitante_act").val(info[3]);
            $("#descripcion_act").val(info[4]);
            $("#causa_act").val(info[5]);
            $("#area_act").val(info[7]);
            $("#solucion_act").val(info[6]);
            $("#tipologia_act").val(info[8]);
            $("#sub_tipologia_act").val(info[9]);
            $("#prioridad_act").val(info[10]);
            $("#estatus_act").val(info[11]);
            $("#registro_act").val(info[0]);
            $("#editar_incidencia").modal("show");
        }
    });
}

function editar2(dato){
    var envio="dato="+dato;

    $.ajax({
        type: "POST",
        data: envio,
        url: "assets/php/consulta_datos.php",

        success:function(respuesta){
            var info=respuesta.split("||");
            $("#clinica_act2").val(info[2]);
            $("#solicitante_act2").val(info[3]);
            $("#descripcion_act2").val(info[4]);
            $("#causa_act2").val(info[5]);
            $("#area_act2").val(info[7]);
            $("#solucion_act2").val(info[6]);
            $("#tipologia_act2").val(info[8]);
            $("#sub_tipologia_act2").val(info[9]);
            $("#prioridad_act2").val(info[10]);
            $("#estatus_act2").val(info[11]);
            $("#registro_act2").val(info[0]);
            $("#editar_incidencia2").modal("show");
        }
    });
}


    //fin editar incidencia

    function actualizacion(){
        var cadena="id_registro="+$("#registro_act").val()+"&id_clinica="+$("#clinica_act").val()+"&solicitante="+$("#solicitante_act").val()+"&descripcion="+$("#descripcion_act").val()+
        "&causa="+$("#causa_act").val()+"&area="+$("#area_act").val()+"&solucion="+$("#solucion_act").val()+
        "&tipologia="+$("#tipologia_act").val()+"&sub="+$("#sub_tipologia_act").val()+"&prioridad="+$("#prioridad_act").val()+"&estatus="+$("#estatus_act").val();
  
        $.ajax({
            type: "POST",
            url: "assets/php/actualizar_incidencia.php",
            data: cadena,
  
            success:function(respuesta){
                if(respuesta==1){
                  alertify.success('Incidencia Actualizada Correctamente');
                  $("#tabla_resumen").load("assets/component/resumen_actualizado.php"); 
                  $("#tabla_indicadores").load("assets/component/indicadores_actualizado.php");
                  $("#editar_incidencia").modal("hide");
                }else{
                  alertify.error('Ocurrio un Error');
  
                }
            }
  
        });
      }

      function actualizacion2(){
        var cadena="id_registro="+$("#registro_act2").val()+"&id_clinica="+$("#clinica_act2").val()+"&solicitante="+$("#solicitante_act2").val()+"&descripcion="+$("#descripcion_act2").val()+
        "&causa="+$("#causa_act2").val()+"&area="+$("#area_act2").val()+"&solucion="+$("#solucion_act2").val()+
        "&tipologia="+$("#tipologia_act2").val()+"&sub="+$("#sub_tipologia_act2").val()+"&prioridad="+$("#prioridad_act2").val()+"&estatus="+$("#estatus_act2").val();
  
        $.ajax({
            type: "POST",
            url: "assets/php/actualizar_incidencia2.php",
            data: cadena,
  
            success:function(respuesta){
                if(respuesta==1){
                  alertify.success('Incidencia Actualizada Correctamente');
                  $("#tabla_resumen").load("assets/component/resumen_actualizado.php"); 
                  $("#tabla_indicadores").load("assets/component/indicadores_actualizado.php");
                  $("#editar_incidencia2").modal("hide");
                }else{
                  alertify.error('Ocurrio un Error');
  
                }
            }
  
        });
      }

function validar_tipologia(){
    var valor=$("#area").val();
    $("#tipologia").load("assets/component/tipologia.php?&tipo="+valor);
}

function validar_sub(){
    var valor2=$("#lista_tipologia").val();
    $("#sub_tipologia").load("assets/component/sub_tipologia.php?&tipo="+valor2);
}

function validar_tipologia_act(){
    var valor3=$("#area_act").val();
    $("#validar_tipologia_act").load("assets/component/tipologia_act.php?&tipo="+valor3);
    
}