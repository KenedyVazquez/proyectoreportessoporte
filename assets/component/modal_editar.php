<div id="editar_incidencia" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="titulo_incidencia" value="">EDICION DE INCIDENCIA</h4>
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <from  id="actualizacion" name="actualizacion" method="POST" action="">
                                                                    <div class="form-group">
                                                                        <div class="col-md-12 m-b-20">
                                                                        <label>Clinica: </label>
                                                                        <select class="custom-select col-12" id="clinica_act2" name="clinica_act2" required>
                                                                        <option value="" selected>Clinica Solicitante</option>
                                                                            <?php
                                                                                    require_once "assets/php/conexion.php";
                                                                                    $conexion=conexion();

                                                                                    $consulta="SELECT * FROM clinica";
                                                                                    $respuesta=mysqli_query($conexion, $consulta);
                                                                                    while($dato=mysqli_fetch_row($respuesta)){
                                                                            ?>
                                                                                <option value="<?php echo $dato[0]; ?>"><?php echo $dato[1]; ?> </option>
                                                                                <?php
                                                                                    }
                                                                                ?>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-md-12 m-b-20">
                                                                        <label>Solicitante: </label>
                                                                            <input type="text" class="form-control" placeholder="Solicitante" id="solicitante_act" name="solicitante_act" required> </div>
                                                                        <div class="col-md-12 m-b-20">
                                                                        <label>Incidencia: </label>
                                                                        <textarea class="form-control" rows="4" placeholder="Descripci&oacute;n" id="descripcion_act" name="descripcion_act" required></textarea> </div>
                                                                        <div class="col-md-12 m-b-20">
                                                                        <label>causa: </label>
                                                                        <input type="text" class="form-control" placeholder="Causa del Problema" id="causa_act" name="causa_act" required> </div>
                                                                        <div class="col-md-12 m-b-20">
                                                                        <label>Area: </label>
                                                                        <select class="custom-select col-12" id="area_act" name="area_act" required>
                                                                            <option value="" selected>Area Responsable</option>

                                                                        <?php
                                                                                require_once "assets/php/conexion.php";
                                                                                $conexion=conexion();

                                                                                $consulta="SELECT * FROM area";
                                                                                $respuesta=mysqli_query($conexion, $consulta);
                                                                                while($dato=mysqli_fetch_row($respuesta)){
                                                                        ?>
                                                                            <option value="<?php echo $dato[0]; ?>"><?php echo $dato[1]; ?> </option>
                                                                            <?php
                                                                                }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                        <div class="col-md-12 m-b-20">
                                                                        <label>Solucion: </label>
                                                                        <textarea class="form-control" rows="4" placeholder="Soluci&oacute;n" id="solucion_act" name="solucion_act" required></textarea> </div>
                                                                        <div class="col-md-12 m-b-20">
                                                                        <label>Tipologia: </label>
                                                                        <select class="custom-select col-12" id="tipologia_act" name="tipologia_act" required>
                                                                        <option value="" selected>Tipologia</option>

                                                                        <?php
                                                                                require_once "assets/php/conexion.php";
                                                                                $conexion=conexion();

                                                                                $consulta="SELECT * FROM tipologia";
                                                                                $respuesta=mysqli_query($conexion, $consulta);
                                                                                while($dato=mysqli_fetch_row($respuesta)){
                                                                        ?>
                                                                            <option value="<?php echo $dato[0]; ?>"><?php echo $dato[1]; ?> </option>
                                                                            <?php
                                                                                }
                                                                            ?>
                                                                        </select>    
                                                                        </div>

                                                                        <div class="col-md-12 m-b-20">
                                                                        <label>Sub-Tipologia: </label>
                                                                        <select class="custom-select col-12" id="sub_tipologia_act" name="sub_tipologia_act" required>
                                                                        <option value="" selected>Sub-Tipologia</option>

                                                                        <?php
                                                                            require_once "assets/php/conexion.php";
                                                                            $conexion=conexion();

                                                                            $consulta="SELECT * FROM sub_tipologia";
                                                                            $respuesta=mysqli_query($conexion, $consulta);
                                                                            while($dato=mysqli_fetch_row($respuesta)){
                                                                        ?>
                                                                        <option value="<?php echo $dato[0]; ?>"><?php echo $dato[1]; ?> </option>
                                                                        <?php
                                                                            }
                                                                        ?>
                                                                    </select>
                                                                        </div>

                                                                        <div class="col-md-12 m-b-20">
                                                                        <label>Prioridad: </label>
                                                                        <select class="custom-select col-12" id="prioridad_act" name="prioridad_act" required>
                                                                        <option selected value="">Prioridad</option>
                                                                        <option value="1">INMEDIATA</option>
                                                                        <option value="2">ALTA</option>
                                                                        <option value="3">MEDIA</option>
                                                                        <option value="4">BAJA</option>
                                                                    </select>
                                                                        
                                                                        </div>

                                                                        <div class="col-md-12 m-b-20">
                                                                        <label>Estatus: </label>
                                                                        <select class="custom-select col-12" id="estatus_act" name="estatus_act" required>
                                                                        <option value="" selected>Estatus</option>

                                                                    <?php
                                                                            require_once "assets/php/conexion.php";
                                                                            $conexion=conexion();

                                                                            $consulta="SELECT * FROM estatus";
                                                                            $respuesta=mysqli_query($conexion, $consulta);
                                                                            while($dato=mysqli_fetch_row($respuesta)){
                                                                    ?>
                                                                        <option value="<?php echo $dato[0]; ?>"><?php echo $dato[1]; ?> </option>
                                                                        <?php
                                                                            }
                                                                        ?>
                                                                    </select>
                                                                        </div>
                                                                        <div class="col-md-12 m-b-20">
                                                                        <input type="text" class="form-control" id="registro_act" name="registro_act" disabled hidden required> 
                                                                    </div>
                                                                       
                                                                    </div>
                                                                </from>
                                                            </div>
                                                            <div class="modal-footer">
                                                            <button type="submit" class="btn btn-primary waves-effect waves-light" id="guardar" onclick="actualizacion()">Guardar</button>
                                                                <button type="submit" class="btn waves-effect waves-light btn-dark btn-reverse"  data-dismiss="modal">Cancelar</button>
                                                            </div>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>








