<table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
    
    <thead>
        <tr>
            <th>No</th>
            <th>Clinica</th>
            <th>Incidencia</th>
            <th>Area Responsable</th>
            <th>Estatus</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>

    <?php
    require_once "assets/php/conexion.php";
    $conexion=conexion();
    $fecha_limite=date("Y-m-d");

    $consulta="SELECT clinica.nombre_clinica, incidencia.descripcion, area.descripcion, estatus.descripcion, incidencia.id_incidencia
    FROM clinica, incidencia, area, estatus 
    WHERE incidencia.id_clinica=clinica.id_clinica and incidencia.id_area=area.id_area and incidencia.id_estatus=estatus.id_estatus and incidencia.fecha_reporte='$fecha_limite' ORDER BY incidencia.id_incidencia ASC";
    $respuesta=mysqli_query($conexion, $consulta);
    
    while($datos=mysqli_fetch_row($respuesta)){
        $informacion=substr($datos[1],0,150);
        $informacion2=strtoupper($informacion);
    ?>
    
        <tr>
            <td><?php echo $datos[4]; ?></td>
        <td><?php echo $datos[0]; ?></td>
        <td><div class="wrap2"><?php echo $informacion2; ?></div></td>
        <td><?php echo $datos[2]; ?></td>
        <td><?php echo $datos[3]; ?></td>
        <td>
        <button type="submit" class="btn btn-primary waves-effect waves-light" id="editar" onclick="editar(<?php echo $datos[4];?>);"><i class="fas fa-edit"></i></button>
        <button type="submit" class="btn btn-dark btn-reverse waves-effect waves-light" id="eliminar" onclick="eliminar(<?php echo $datos[4];?>)"><i class="fas fa-trash"></i></button>
        </td>
        </tr>
    <?php
    }
    ?>                                
    </tbody>
</table>