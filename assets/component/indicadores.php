    <?php
    require_once "assets/php/conexion.php";
    $conexion=conexion();
    
    $fecha_limite= date("Y-m-d");
    $consulta_estatus="SELECT *FROM incidencia WHERE fecha_reporte='$fecha_limite'";
    $datos=mysqli_query($conexion, $consulta_estatus);
    $total_registros=mysqli_num_rows($datos);
    $registro_pendientes=0;
    $registro_proceso=0;
    $registro_resuelto=0;

    while($informacion=mysqli_fetch_row($datos)){
       if($informacion[11]=="1"){
           $registro_pendientes=$registro_pendientes+1;
            
       }elseif($informacion[11]=="8"){
           $registro_proceso=$registro_proceso+1;

       }elseif($informacion[11]=="3"){
           $registro_resuelto=$registro_resuelto+1;
       }
    }
    ?>

<div class="row m-t-40">
                                    <!-- Column -->
                                    <div class="col-md-6 col-lg-3 col-xlg-3">
                                        <div class="card card-inverse card-info">
                                            <div class="box bg-info text-center">
                                                <h1 class="font-light text-white"><?php
                                                echo $total_registros;
                                                ?></h1>
                                                <h6 class="text-white">Total de Incidencias</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Column -->
                                    <div class="col-md-6 col-lg-3 col-xlg-3">
                                        <div class="card card-primary card-inverse">
                                            <div class="box text-center">
                                                <h1 class="font-light text-white">
                                                    <?php
                                                    echo $registro_resuelto;
                                                    ?>
                                                </h1>
                                                <h6 class="text-white">Solucionadas</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Column -->
                                    <div class="col-md-6 col-lg-3 col-xlg-3">
                                        <div class="card card-inverse card-success">
                                            <div class="box text-center">
                                                <h1 class="font-light text-white">
                                                    <?php
                                                    echo $registro_proceso;
                                                    ?>
                                                </h1>
                                                <h6 class="text-white">Solucionados por el Momentoo</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Column -->
                                    <div class="col-md-6 col-lg-3 col-xlg-3">
                                        <div class="card card-inverse card-dark">
                                            <div class="box text-center">
                                                <h1 class="font-light text-white">
                                                    <?php
                                                    echo $registro_pendientes;
                                                    ?>
                                                </h1>
                                                <h6 class="text-white">Pendientes</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Column -->
                                </div>







                                <div class="table-responsive">
                                    <table id="tabla_general" class="table m-t-30 table-hover no-wrap contact-list .table-bordered td, .table-bordered th" data-page-size="10">
                                        <thead>
                                            <tr>
                                                <th>FECHA</th>
                                                <th>CLINICA QUIEN SOLICITA</th>
                                                <th>QUIEN SOLICITA</th>
                                                <th>DESCRIPCION REPORTE</th>
                                                <th>CAUSA</th>
                                                <th>DESCRIPCION SOLUCION</th>
                                                <th>AREA RESPONSABLE</th>
                                                <th>TIPOLOGIA</th>
                                                <th>SUB-TIPOLOGIA</th>
                                                <th>ESTATUS</th>
                                                <th>PRIORIDAD</th>
                                                <th>FECHA SOLUCION</th>
                                                <th>OPCIONES</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $consulta_datos="SELECT incidencia.id_incidencia, incidencia.fecha_reporte, clinica.nombre_clinica, incidencia.descripcion, incidencia.causa, incidencia.solucion, area.descripcion, tipologia.descripcion, sub_tipologia.descripcion, estatus.descripcion, incidencia.solicitante, prioridad.descripcion FROM incidencia,clinica, area, tipologia, sub_tipologia, estatus, prioridad WHERE incidencia.id_clinica=clinica.id_clinica and incidencia.id_area=area.id_area and incidencia.id_tipologia=tipologia.id_tipologia and incidencia.id_sub=sub_tipologia.id_sub and incidencia.id_estatus=estatus.id_estatus and incidencia.id_prioridad=prioridad.id_prioridad ORDER BY fecha_reporte DESC ";
                                            $respuesta_datos=mysqli_query($conexion, $consulta_datos);

                                            while($cadena=mysqli_fetch_row($respuesta_datos)){
                                            ?>


     
                                            <tr>
                                            <td><?php echo $cadena[1];?></td>
                                            <td><?php echo $cadena[2];?></td>
                                            <td><?php echo $cadena[10];?></td>
                                            <td class="warp3"><?php echo strtoupper(substr($cadena[3],0,150));?></td>
                                            <td class="warp3"><?php echo strtoupper(substr($cadena[4],0,150));?></td>
                                            <td class="warp3"><?php echo strtoupper(substr($cadena[5],0,150));?></td>
                                            <td><?php echo $cadena[6];?></td>
                                            <td><?php echo $cadena[7];?></td>
                                            <td><?php echo $cadena[8];?></td>
                                            <td><span
                                                <?php
                                                if($cadena[9]=="PENDIENTE"){
                                                    echo 'class="label label-dark label-inverse"';
                                                }else if($cadena[9]=="SOLUCIONADO POR EL MOMENTO"){
                                                    echo 'class="label label-success"';
                                                }else if($cadena[9]=="SOLUCIONADO"){
                                                    echo 'class="label label-primary"';
                                                }
                                                ?>
                                                ><?php echo $cadena[9];?></span> </td>
                                                <td><?php echo $cadena[11];?></td>
                                                <td>
                                                    <?php
                                                    if($cadena[9]=="SOLUCIONADO" || $cadena[9]=="SOLUCIONADO POR EL MOMENTO"){
                                                        echo $cadena[1];
                                                    }
                                                    ?>
                                                </td>
                                            <td>
                                                <button type="button" class="btn waves-effect waves-light btn-primary" id="editar" onclick="editar2(<?php echo $cadena[0]?>)"><i class="fas fa-edit"></i></button>
                                                <button type="button" class="btn waves-effect waves-light btn-dark btn-reverse" id="eliminar" onclick="eliminar(<?php echo $cadena[0];?>)"><i class="fas fa-trash"></i></button>
                                                </td>
                                            </tr>
                                            <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                    
                                </div>
                                