<!DOCTYPE html>
<html lang="en">

<?php
require_once ("assets/component/cabecera.php");
?>
 
<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper">
        <div class="login-register" style="background-image:url(assets/images/background/login-register.jpg);">        
            <div class="login-box card">
            <div class="card-block">
                <form class="form-horizontal form-material" id="registro_usuario" action="" method="post">
                    <h3 class="box-title m-b-20">Registrar</h3>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" id="nombre" name="nombre" required="" placeholder="Nombre" onkeypress="return  soloLetras(event)">
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" id="paterno" name="paterno" required="" placeholder="Paterno" onkeypress="return  soloLetras(event)">
                        </div>
                    </div>
                   
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" id="materno" name="materno" required="" placeholder="Materno" onkeypress="return  soloLetras(event)">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                        <select class="custom-select col-12" id="grupo" name="grupo" required>
                        <option value="" selected>Grupo de Acompañamiento</option>
                        <option value="1"> Grupo 1 </option>
                        <option value="2"> Grupo 2 </option>
                        <option value="3"> Grupo 3 </option>
                        </select>

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" id?="usuario" name="usuario" required="" placeholder="Usuario" onkeypress="return  numYletras(event)">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" type="password" id="clave" name="clave" required="" placeholder="Clave" onkeypress="return  numYletras(event)" maxlength="12">
                        </div>
                    </div>
                    
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit" id="registrar" name="registrar">Registrar</button>
                        </div>
                    </div>
                   
                </form>
                
            </div>
          </div>
        </div>
        
    </section>
    <?php
    require_once("assets/component/js.php");
    ?>
</body>

</html>